# Quiz App with vanilla HTML, CSS, and JavaScript

![Home Screen](./images/cover.png)

This quiz application is a **personal project** I built using James Q Quick's video course at Learn Build Teach.

Thanks to this project I worked on at the very begining of my carreer change, I improved my **core Web Develoment skills**, namely my knowledge of **HTML, CSS, and JavaScript**. I learned how to build an application **without the assistance of libraries or frameworks**.

## Technologies used

- HTML
- CSS
- JavaScript

## Skills gained

- Dynamically generate HTML in JavaScript
- JavaScript : Array Functions (splice, map, sort), Local Storage, Fetch API
- ES6 JavaScript Features : Arrow Functions, the Spread Operator, Const and Let, Template Literals
- HTML : Emmet abbreviations
- CSS : Flexbox, Animations, and REM units

## Functionalities

- Load questions from Open Trivia DB API
- Save high scores in Local Storage

## Credit

- [Course Source Code](https://github.com/jamesqquick/Design-And-Build-A-Quiz-App)
- [Video Course](https://www.youtube.com/playlist?list=PLB6wlEeCDJ5Yyh6P2N6Q_9JijB6v4UejF)
